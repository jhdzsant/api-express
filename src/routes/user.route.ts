import { Router } from "express";

import { GetUsers, createUser, getUser, deleteUser, updateUser } from "../controllers/users.controllers";
import { TokenValidation } from '../lib/verifyToken';

const router = Router();
router.route('/')
        .get(TokenValidation,GetUsers)
        .post(createUser);

router.route('/:postId')
        .get(TokenValidation,getUser)
        .delete(TokenValidation,deleteUser)
        .put(TokenValidation,updateUser);

export default router;