import { Router } from "express";

import { GetBrands, createBrand, getBrand, deleteBrand, updateBrand } from "../controllers/brands.controllers"
const router = Router();
router.route('/')
        .get(GetBrands)
        .post(createBrand);

router.route('/:postId')
        .get(getBrand)
        .delete(deleteBrand)
        .put(updateBrand);

export default router;