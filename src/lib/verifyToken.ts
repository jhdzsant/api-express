import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export interface IPayload {
    _id: string;
    iat: number;
} 

export async function TokenValidation(req: Request, res: Response,next: NextFunction) {
    try {
        
        const token = req.header('token');
        console.log(token);
        if (!token) return res.status(401).json('Access Denied');
        const payload = jwt.verify(token, process.env['TOKEN_SECRET'] || '123456') as IPayload;
        //res.header('token', token).json(token);
        next();
    } catch (e) {
        res.status(400).send('Invalid Token');
        console.log(e);
    }
}