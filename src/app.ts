import express, { Application } from 'express';
import morgan from "morgan";

//Routes
import IndexRote from "./routes/index.route";
import BrandAPI from "./routes/brands.route";
import UserAPI from "./routes/user.route";
import AuthAPI from "./routes/auth.route";
import cors from "cors";

export class App {
    private app: Application;
    constructor(private port?: number | string) {
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
    }

    settings(){
        this.app.set('port', this.port || process.env.PORT || 3000)
    }

    middlewares(){
        const corsOptions = {
            exposedHeaders: 'token',
          };

        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(cors(corsOptions));
    }

    routes(){
        this.app.use(IndexRote);
        this.app.use('/api/brands',BrandAPI);
        this.app.use('/api/users',UserAPI);
        this.app.use('/api/auth', AuthAPI);
    }
    swagger(){

    }

    async listen() {
        await this.app.listen(this.app.get('port'));
        console.log("Server port on: ",this.app.get('port'));
    }

}