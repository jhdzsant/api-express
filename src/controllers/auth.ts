import { Request, Response } from 'express';
import { IUser, User } from '../interface/user.interface';
import { signupValidation, signinValidation } from '../lib/joi';
import jwt from 'jsonwebtoken';
import { connect } from "../database";

export async function signup(req: Request, res: Response) {
    // // Validation
    // const { error } = signupValidation(req.body);
    // if (error) return res.status(400).json(error.message);

    // // Email Validation
    // const emailExists = await User.findOne({ email: req.body.email });
    // if (emailExists) return res.status(400).json('Email already exists');

    // // Saving a new User
    // try {
    //     const newUser: IUser = new User({
    //         username: req.body.username,
    //         email: req.body.email,
    //         password: req.body.password
    //     });
    //     //newUser.password = await newUser.encrypPassword(newUser.password);
    //     newUser.password = newUser.password;
    //     const savedUser = await newUser;

    //     const token: string = jwt.sign({ _id: savedUser.id }, process.env['TOKEN_SECRET'] || '', {
    //         expiresIn: 60 * 60 * 24
    //     });
    //     // res.header('auth-token', token).json(token);
    //     res.header('auth-token', token).json(savedUser);
    // } catch (e) {
    //     res.status(400).json(e);
    // }
    res.json("signup");
};

export async function signin(req: Request, res: Response) {
    const user: User = req.body;
    const conn = await connect();
    const [userResult] = await conn.query<User[]>('SELECT * FROM Users WHERE Email= ? and Password = ?', [user.email,user.password]);
    let objUser : User = userResult[0];
    if (objUser == undefined || objUser == null) return res.status(400).json("Email or Password is wrong");
    // // Create a Token
    const token: string = await jwt.sign({ _id: user.id}, process.env['TOKEN_SECRET'] || '123456');
    res.status(200).header('token', token).json(objUser);
};

export const profile = async (req: Request, res: Response) => {

    // const user = await User.findById("1023455", { password: 0 });
    // if (!user) {
    //     return res.status(404).json('No User found');
    // }
    res.json("Profile");
};