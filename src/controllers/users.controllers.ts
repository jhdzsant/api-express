import { Request, Response } from "express";
import { connect } from "../database";
import { User } from "../interface/user.interface";

export async function GetUsers(req: Request, res: Response): Promise<Response> {
    const conn = await connect();
    const Users = await conn.query('SELECT * FROM Users');
    return res.json(Users[0]);
}


export async function createUser(req: Request, res: Response) {
    const newUser: User = req.body;
    const conn = await connect();
    const [posts] = await conn.query<User[]>('SELECT * FROM Users WHERE email = ?', [newUser.email]);
    const validamosUser: User = posts[0];
    if (validamosUser != null) return res.status(400).json("User is exist");
    if (newUser != undefined) {
        await conn.query('INSERT INTO Users SET ?', [newUser]).then(
            r => {
                console.log("Result:", r);
                res.json({
                    message: 'New User Created'
                });
            }
        ).catch(e => console.log("Error: ", e));

    } else {
        res.json({
            message: 'don´t create'
        });
    }

}

export async function getUser(req: Request, res: Response) {
    const id = req.params.postId;
    const conn = await connect();
    const posts = await conn.query('SELECT * FROM Users WHERE Id = ?', [id]);
    res.json(posts[0]);
}

export async function deleteUser(req: Request, res: Response) {
    const id = req.params.postId;
    const conn = await connect();
    await conn.query('DELETE FROM Users WHERE Id = ?', [id]);
    res.json({
        message: 'User deleted'
    });
}

export async function updateUser(req: Request, res: Response) {
    const id = req.params.postId;
    const updateUser: User = req.body;
    const conn = await connect();
    await conn.query('UPDATE Users set ? WHERE Id = ?', [updateUser, id]);
    res.json({
        message: 'User Updated'
    });
}