import { Request, Response } from "express";
import { connect } from "../database";
import { Brand } from "../interface/brand.interface";

export async function GetBrands(req: Request, res: Response): Promise<Response> {
    const conn = await connect();
    const brands = await conn.query('SELECT * FROM Brands');
    return res.json(brands[0]);
}


export async function createBrand(req: Request, res: Response) {
    const newBrand: Brand = req.body;
    const conn = await connect();
    if (newBrand != undefined) {
        await conn.query('INSERT INTO Brands SET ?', [newBrand]).then(
            r => {
                console.log("Result:", r);
                res.json({
                    message: 'New Brand Created'
                });
            }
        ).catch(e => console.log("Error: ", e));

    } else {
        res.json({
            message: 'don´t create'
        });
    }

}

export async function getBrand(req: Request, res: Response) {
    const id = req.params.postId;
    const conn = await connect();
    const posts = await conn.query('SELECT * FROM Brands WHERE Id = ?', [id]);
    res.json(posts[0]);
}

export async function deleteBrand(req: Request, res: Response) {
    const id = req.params.postId;
    const conn = await connect();
    await conn.query('DELETE FROM Brands WHERE Id = ?', [id]);
    res.json({
        message: 'Brand deleted'
    });
}

export async function updateBrand(req: Request, res: Response) {
    const id = req.params.postId;
    const updateBrand: Brand = req.body;
    const conn = await connect();
    await conn.query('UPDATE Brands set ? WHERE Id = ?', [updateBrand, id]);
    res.json({
        message: 'Brand Updated'
    });
}