import bcrypt from 'bcryptjs';
import { RowDataPacket } from 'mysql2';

export interface IUser extends Document {
    username: string;
    email: string;
    password: string;
    // encrypPassword(password: string): Promise<string>;
    // validatePassword(password: string): Promise<boolean>;
};
export interface User extends RowDataPacket {
    id: {
        type: number,
        required: false,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: false
    }
}



// userSchema.methods.encrypPassword = async (password: string): Promise<string> => {
//     const salt = await bcrypt.genSalt(10);
//     return bcrypt.hash(password, salt);
// };

// userSchema.methods.validatePassword = async function (password: string): Promise<boolean> {
//     return await bcrypt.compare(password, this.password);
// };
